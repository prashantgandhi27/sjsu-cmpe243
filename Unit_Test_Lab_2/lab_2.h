/* Unit test Lab_2.h
 * created by Prashant Shushilbhai Gandhi
 * Date: 03/03/2019
 * This is an circuilar queue
 * reference: "Data structure using C++" by D.S.Malik
 */

#include <stdint.h>
#include <stdbool.h>
#include  <stdio.h>

#define max_size 100

typedef struct {
  uint8_t queue_memory[max_size];
  uint8_t queue_front;
  uint8_t queue_rear;
  uint8_t count;
} queue_S;

void queue__init(queue_S *queue);

bool queue__push(queue_S *queue, uint8_t push_value);

bool queue__pop(queue_S *queue, uint8_t *pop_value);

size_t queue__get_count(queue_S *queue);
