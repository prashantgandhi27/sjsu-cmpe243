/* Unit test Lab_2.h
 * created by Prashant Shushilbhai Gandhi
 * Date: 03/03/2019
 * This is an CIRCUILAR QUEUE
 * reference: "Data structure using C++" by D.S.Malik
 */

#include <stdint.h>
#include <stdbool.h>
#include  <stdio.h>
#include "lab_2.h"

void queue__init(queue_S *queue)
{
    queue->count = 0;
    queue->queue_front = 0;
    queue->queue_rear = max_size-1;
}

bool queue__push(queue_S *queue, uint8_t push_value)
{
    bool ret;
    if(!(queue->count == max_size))
    {
        queue->queue_rear = (queue->queue_rear + 1) % max_size;
        queue->count++;
        queue->queue_memory[queue->queue_rear] = push_value;
        ret = true;
    }else{
        ret = false;
    }

    return ret;
}

bool queue__pop(queue_S *queue, uint8_t *pop_value)
{
    bool ret;
    if(queue->count != 0){
        queue->count--;
        *pop_value = queue->queue_memory[queue->queue_front];
        queue->queue_front = (queue->queue_front + 1) % max_size;
        ret = true;
    }else{
        ret = false;
    }
    return ret;
}

size_t queue__get_count(queue_S *queue)
{
    return queue->count;
}
