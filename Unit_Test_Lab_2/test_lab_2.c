#include "unity.h"
#include "lab_2.h"

void setUp(void) {

}

void tearDown(void) {

}

void test_check_queue_init(void){
    queue_S q_input_1={};
    queue__init(&q_input_1);
    TEST_ASSERT_EQUAL(0,q_input_1.count);
    TEST_ASSERT_EQUAL(0,q_input_1.queue_front);
    TEST_ASSERT_EQUAL(99,q_input_1.queue_rear);

}

void test_check_queue_push(void){
    queue_S q_input_1={};
    queue_S q_input_2={};
    uint8_t push_value = 70;

    for(int i = 0; i<50; i++)
    {
        TEST_ASSERT_TRUE(queue__push(&q_input_1, push_value));
    }

    for(int i = 0; i<100; i++)
    {
        TEST_ASSERT_TRUE(queue__push(&q_input_2, push_value));
    }

}

void test_check_queue_pop(void){
    queue_S q_input_1={};

    uint8_t pop_value;
    uint8_t push_value_1 = 70;
    uint8_t push_value_2 = 25;
    uint8_t push_value_3 = 10;

    for(int i = 0; i<25; i++)    // Queue elements from 0-24 will be 70
    {
        TEST_ASSERT_TRUE(queue__push(&q_input_1, push_value_1));
    }

    for(int i = 0; i<10; i++)    // Queue elements from 25-34 will be 25
    {
        TEST_ASSERT_TRUE(queue__push(&q_input_1, push_value_2));
    }

    for(int i = 0; i<20; i++)    // Queue elements from 35-54 will be 10
    {
        TEST_ASSERT_TRUE(queue__push(&q_input_1, push_value_3));
    }

    for(int i = 0; i<30; i++)    // Extracting 30th element which should be 25
    {
        TEST_ASSERT_TRUE(queue__pop(&q_input_1, &pop_value));
    }

        TEST_ASSERT_EQUAL(25,pop_value);

    // Test should fail as we are pushing 50 elements into queue but trying to extracting 51 elements
    /*queue_S q_input_2={};
    for(int i = 0; i<50; i++)
    {
        TEST_ASSERT_TRUE(queue__push(&q_input_2, push_value));
    }

    for(int i = 0; i<51; i++)
    {
        TEST_ASSERT_TRUE(queue__pop(&q_input_2, &pop_value));
    }*/
}

void test_check_queue_get_count(void){
    queue_S q_input_1={};
    uint8_t push_value = 70;

    for(int i = 0; i<50; i++)
    {
        TEST_ASSERT_TRUE(queue__push(&q_input_1, push_value));
    }

    TEST_ASSERT_EQUAL(50,queue__get_count(&q_input_1));


    queue_S q_input_2={};

    for(int i = 0; i<100; i++)
    {
        TEST_ASSERT_TRUE(queue__push(&q_input_2, push_value));
    }

    TEST_ASSERT_EQUAL(100,queue__get_count(&q_input_2));
}
